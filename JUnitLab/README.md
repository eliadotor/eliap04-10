En la clase Subscripcion tenemos creados un constructor y los métodos `precioPorMes()` y `cancel()`, por ello en la clase *SubscripcionTest* vamos a hacer varios test para comprobar cada método.

En el caso del método `precioPorMes()`, hacemos un `testPrecioPorMes()` en el que hacemos que ni el *periodo* ni el *precio* sean iguales o menores que 0, entrando de esta manera en el else:

![precio](imagenes/clonoRepositorio.png)